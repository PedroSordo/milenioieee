Rails.application.routes.draw do
  root 'users#new'
  get '/confirmation' => 'sections#confirmation', as: 'confirm'
  resources :users, except: [:destroy]

  namespace :admin do
    get '/login' => 'sessions#new'
    post 'admin/login' => 'sessions#create'
    get '/logout' => 'sessions#destroy'
    resources :users, only: [:index]
  end
end
