class ApplicationMailer < ActionMailer::Base
  default from: "al02694147@tecmilenio.mx"
  layout 'mailer'
end
