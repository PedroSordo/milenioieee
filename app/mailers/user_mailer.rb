class UserMailer < ApplicationMailer
  default from: "al02694147@tecmilenio.mx"

  def welcome_email(user)
    @user = user
    mail(to: "#{user.coda}@tecmilenio.mx",
      subject: 'Confirmación de Registro',
      track_opens: 'true')
  end
end
