class User < ActiveRecord::Base
  validates :first_name, presence: true, length: { minimum: 2 }
  validates :last_name, presence: true, length: { minimum: 2 }
  validates :coda, presence: true, length: { maximum: 10  }, uniqueness: true,
    format: { with: (/[a+l]+[0-9]{8}/) }

  def self.to_csv(users = {})
    CSV.generate(users) do |csv|
      csv << column_names
      all.each do |user|
        csv << user.attributes.values_at(*column_names)
      end
    end
  end
end
