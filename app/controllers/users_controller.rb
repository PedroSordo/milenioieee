class UsersController < ApplicationController
  respond_to :html
  helper_method :talleres
  def new
    @user = User.new
    talleres
  end
  def create
    @user = User.new(permited_params)
    if @user.save
      UserMailer.welcome_email(@user).deliver_now
      flash[:notice] = "Te has registrado con éxito"
      redirect_to confirm_path
    else
      respond_with @user
    end
  end

  private
  def permited_params
    params.require(:user).permit(:first_name, :last_name, :coda, :workshop)
  end

  def talleres
    @talleres = [ "Festo", "CEBETI", "CARPLASTIC", "AGENCIA ESPACIAL MEXICANA" ]
  end
end
