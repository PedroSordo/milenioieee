class Admin::SessionsController < ApplicationController
  before_action :already_on, except: [:destroy]
  def new
  end

  def create
    admin = Admin.find_by_email(params[:email])
    if admin && admin.authenticate(params[:password])
      session[:admin_id] = admin.id
      flash[:notice] = "Bienvenido #{admin.first_name}"
      redirect_to admin_users_path
    else
      flash[:error] = "Email o contraseña incorrecta."
      redirect_to admin_login_path
    end
  end

  def destroy
    session[:admin_id] = nil
    flash[:notice] = "Sesión terminada exitosamente."
    redirect_to admin_login_path
  end
end
