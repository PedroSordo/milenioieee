class Admin::UsersController < ApplicationController
  before_action :admin_auth

  def index
    @users = User.all
    respond_to do |format|
      format.html
      format.csv { send_data @users.to_csv }
    end
  end
end
