class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :admin_auth, :already_on

  def current_admin
    @current_admin ||= Admin.find(session[:admin_id]) if session[:admin_id]
  end

  def already_on
    redirect_to admin_users_path if current_admin
  end

  def admin_auth
    redirect_to admin_login_path unless current_admin
  end
end
